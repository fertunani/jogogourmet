﻿using System.Windows.Forms;

namespace JogoGourmet.Classes
{
    public static class Game
    {
        private const string ROOT_DISH_DESCRIPTION = "uma massa";
        private const string RIGHT_CHILD_DESCRIPTION = "Lasanha";
        private const string LEFT_CHILD_DESCRIPTION = "Bolo de chocolate";
        private const string DIALOG_TEXT = "Pense em um prato que gosta.";
        private const string DIALOG_CAPTION = "Jogo Gourmet";

        public static void Play()
        {
            var root = CreateRoot();
            var result = DialogResult.OK;

            while (result == DialogResult.OK)
            {
                result = MessageBox.Show(DIALOG_TEXT, DIALOG_CAPTION, MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

                if (result == DialogResult.OK)
                {
                    root.NextNode(null, result);
                }
            }
        }

        private static Node CreateRoot()
        {
            var root = new Dish(null, null, ROOT_DISH_DESCRIPTION);
            root.SetRightChild(new Dish(null, null, RIGHT_CHILD_DESCRIPTION));
            root.SetLeftChild(new Dish(null, null, LEFT_CHILD_DESCRIPTION));

            return root;
        }
    }
}
