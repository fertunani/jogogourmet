﻿using System.Windows.Forms;

namespace JogoGourmet.Classes
{
    public abstract class Node
    {
        private const string DIALOG_TEXT = "O prato que pensou é {0} ?";
        protected const string DIALOG_CAPTION = "Jogo Gourmet";

        protected Node RightChild { get; private set; }
        protected Node LeftChild { get; private set; }
        protected string DishDescription { get; private set; }

        protected Node(Node rightChild, Node leftChild, string dishDescription)
        {
            this.RightChild = rightChild;
            this.LeftChild = leftChild;
            this.DishDescription = dishDescription;
        }

        public void SetRightChild(Node child)
        {
            this.RightChild = child;
        }

        public void SetLeftChild(Node child)
        {
            this.LeftChild = child;
        }

        protected DialogResult Ask()
        {
            return MessageBox.Show(string.Format(DIALOG_TEXT, DishDescription), DIALOG_CAPTION, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }

        public abstract void NextNode(Node parent, DialogResult lastAnswer);
    }
}
