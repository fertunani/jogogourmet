﻿using JogoGourmet.Forms;
using System.Windows.Forms;

namespace JogoGourmet.Classes
{
    public class Dish : Node
    {
        private const string NEW_DISH_QUESTION = "Qual prato você pensou?";
        private const string NEW_DISH_CHARACTERISTIC_QUESTION = "{0} é ________ mas {1} não.";
        private const string NO_DISH_WAS_REPORTED = "Nenhum prato foi informado. Vamos reiniciar o jogo sem cadastrar seu prato.";
        private const string NO_DISH_CHARACTERISTIC_WAS_REPORTED = "Não foi informada a característica do prato. Vamos reiniciar o jogo sem cadastrar o mesmo.";
        private const string ATTENTION_CAPTION = "Atenção";
        private const string GOT_IT_TEXT = "Acertei!";

        public Dish(Node rightChild, Node leftChild, string dishDescription) : base(rightChild, leftChild, dishDescription)
        {
        }

        public override void NextNode(Node parent, DialogResult lastAnswer)
        {
            var answer = Ask();

            if (answer == DialogResult.Yes)
            {
                if (this.RightChild == null)
                {
                    MessageBox.Show(GOT_IT_TEXT, DIALOG_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    this.RightChild.NextNode(this, answer);
                }
            }
            else
            {
                if (this.LeftChild == null)
                {
                    var newDishDescription = GetNewDishDescription();
                    if (string.IsNullOrEmpty(newDishDescription))
                    {
                        MessageBox.Show(NO_DISH_WAS_REPORTED, ATTENTION_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                    var newDishCharacteristic = GetNewDishCharacteristic(newDishDescription, DishDescription);
                    if (string.IsNullOrEmpty(newDishCharacteristic))
                    {
                        MessageBox.Show(NO_DISH_CHARACTERISTIC_WAS_REPORTED, ATTENTION_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                    var newDish = new Dish(null, null, newDishDescription);

                    if (lastAnswer == DialogResult.Yes)
                    {
                        parent.SetRightChild(new Dish(newDish, this, newDishCharacteristic));
                    }
                    else
                    {
                        parent.SetLeftChild(new Dish(newDish, this, newDishCharacteristic));
                    }
                }
                else
                {
                    this.LeftChild.NextNode(this, answer);
                }
            }
        }

        private string GetNewDishCharacteristic(string newDish, string previousDish)
        {
            return ShowInputBox(DIALOG_CAPTION, string.Format(NEW_DISH_CHARACTERISTIC_QUESTION, newDish, previousDish));
        }

        private string GetNewDishDescription()
        {
            return ShowInputBox(DIALOG_CAPTION, NEW_DISH_QUESTION);
        }

        private string ShowInputBox(string caption, string question)
        {
            var inputBox = new InputBox(caption, question);
            inputBox.ShowDialog();

            return inputBox.Answer;
        }
    }
}
