﻿using System;
using System.Windows.Forms;

namespace JogoGourmet.Forms
{
    public partial class InputBox : Form
    {
        private const string DIALOG_CAPTION = "Atenção";
        private const string DIALOG_TEXT = "Por favor, responda a questão.";
        private readonly string caption;
        private readonly string question;

        public string Answer { get; private set; }

        public InputBox()
        {
            InitializeComponent();
        }

        public InputBox(string caption, string question) : this()
        {
            this.caption = caption;
            this.question = question;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txbAnswer.Text) || string.IsNullOrWhiteSpace(txbAnswer.Text))
            {
                MessageBox.Show(DIALOG_TEXT, DIALOG_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            Answer = txbAnswer.Text;
            Close();
        }

        private void InputBox_Shown(object sender, EventArgs e)
        {
            this.Text = caption;
            this.lblQuestion.Text = question;
        }
    }
}
